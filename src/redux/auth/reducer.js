import {
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGOUT_USER
} from "../actions";

const INIT_STATE = {
  user: sessionStorage.getItem("user_id"),
  token: sessionStorage.getItem("token"),
  email: sessionStorage.getItem("email"),
  loading: false,
  error: ""
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case LOGIN_USER:
      return { ...state, loading: true, error: "" };
    case LOGIN_USER_SUCCESS:
      return { ...state, loading: false, user: action.payload._id, email: action.payload.username, error: "" };
    case LOGOUT_USER:
      return { ...state, user: null, email: null, error: "" };
    default:
      return { ...state };
  }
};
