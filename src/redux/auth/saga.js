import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { auth } from "../GateKeeper";
import { LOGIN_USER, LOGOUT_USER } from "../actions";
import { loginUserSuccess} from "./actions";


export function* watchLoginUser() {
  yield takeEvery(LOGIN_USER, loginWithEmailPassword);
}

const loginWithEmailPasswordAsync = async (email, password) =>
  await auth(email, password)
    .then((authUser) => authUser)
    .catch((err) => err);

function* loginWithEmailPassword({ payload }) {
  const { email, password } = payload.user;
  
  const { history } = payload;
  try {
    const loginUser = yield call(loginWithEmailPasswordAsync, email, password);
    
    if (!loginUser.message) {
      let userInfo = {
        id: loginUser.user._id,
        fullname: loginUser.user.fullname,
        email: loginUser.user.username,
      };
      sessionStorage.setItem("user_id", loginUser.user._id);
      sessionStorage.setItem("email", loginUser.user.username)
      sessionStorage.setItem("token", loginUser.token);
      sessionStorage.setItem("user_info", JSON.stringify(userInfo));
      yield put(loginUserSuccess(loginUser.user));
     
    } else {
      console.log('error')
    }
  } catch (error) {
    console.log(error)
  }
}


export function* watchLogoutUser() {
  yield takeEvery(LOGOUT_USER, logout);
}

const logoutAsync = async (history) => {
  window.sessionstorage.clear();
  localStorage.clear()
};

function* logout({ payload }) {
  const { history } = payload;
  try {
    yield call(logoutAsync, history);
  } catch (error) {
    
  }
}

export default function* rootSaga() {
  yield all([
    fork(watchLoginUser),
    fork(watchLogoutUser)
  ]);
}
