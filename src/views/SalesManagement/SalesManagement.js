import React, { useState, useEffect } from "react";
// @material-ui/core Components

// core Components
import GridItem from "../../Components/Grid/GridItem.js";
import GridContainer from "../../Components/Grid/GridContainer.js";
//
import DataTableSalesOrderHeader from "../../Components/DataTableSalesOrderHeader";
import FormRegisterSalesOrderDialog from "../../Components/FormRegisterSalesOrderDialog";
import FormUpdateSalesOrderDialog from "../../Components/FormUpdateSalesOrderDialog";
import ViewConfirmSalesOrderDialog from "../../Components/ViewConfirmSalesOrderDialog";
import ViewCancelSalesOrderDialog from "../../Components/ViewCancelSalesOrderDialog";
import ExtendPaymentDialog from "../../Components/ExtendPaymentDialog";
import AskingCanceltDialog from "../../Components/AskingCanceltDialog";
import {
  URL_IRINA,
  URL_ZARA,
  USERNAME_CONTS,
  PASSWORD_CONTS,
} from "../../config";
import { connect } from "react-redux";
import { loginUser } from "../../redux/actions";
import axios from "axios";

function SalesManagement(props) {
  const [modal, setModal] = useState(false);
  const [OpenAlertProductManagement, setOpenAlertProductManagement] = useState(
    false
  );
  const [modalRegisterSalesOrder, setModalRegisterSalesOrder] = useState(false);
  const [modalUpdateSalesOrder, setModalUpdateSalesOrder] = useState(false);
  const [modalViewConfirmSalesOrder, setModalViewConfirmSalesOrder] = useState(
    false
  );
  const [modalViewCancelSalesOrder, setModalViewCancelSalesOrder] = useState(
    false
  );
  const [modalExtendPayment, setModalExtendPayment] = useState(false);
  const [modalAskingCancel, setModalAskingCancel] = useState(false);

  const [formState, setFormState] = useState({
    status: "",
  });

  const [formStateHalalCert, setFormStateHalalCert] = useState({
    halal_cert_no: "",
    certifying_body: "",
    halalcert_id: "",
    expiry_date: new Date(),
  });

  // const onDeleteHalalCert = (e) => {
  //   const { halalcert_id } = formStateHalalCert;
  //   e.preventDefault();
  //   axios
  //     .delete(`${URL_ZARA}/halalcert/${halalcert_id}`)
  //     .then((result) => {
  //       if (result.status === 200) {
  //         setDeleteHalalCert(true);
  //       }
  //     })
  //     .catch((err) => console.log(err));
  // };

  const handleTextFieldChange = (e) => {
    setFormState({
      ...formState,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    axios
      .put(
        `${URL_IRINA}/product`,
        {
          ...formState,
        },
        {
          headers: {
            Authorization: `Bearer ${props.token}`,
          },
        }
      )
      .then((result) => {
        if (result.status === 200) {
          // setModal(false);
          setOpenAlertProductManagement(true);
          console.log("success", result);
        }
      })
      .catch((err) => console.log("error", err));
  };

  const toggle = (e) => {
    setModal(!modal);
    setFormState(e);
  };

  const toggleRegisterSalesOrder = () => {
    setModalRegisterSalesOrder(!modalRegisterSalesOrder);
  };

  const toggleUpdateSalesOrder = () => {
    setModalUpdateSalesOrder(!modalUpdateSalesOrder);
  };

  const toggleViewConfirmSalesOrder = () => {
    setModalViewConfirmSalesOrder(!modalViewConfirmSalesOrder);
  };

  const toggleViewCancelSalesOrder = () => {
    setModalViewCancelSalesOrder(!modalViewCancelSalesOrder);
  };

  const toggleExtendPayment = () => {
    setModalExtendPayment(!modalExtendPayment);
  };

  const toggleAskingCancel = () => {
    setModalAskingCancel(!modalAskingCancel);
  };

  useEffect(() => {
    const fetchData = async () => {
      props.loginUser({ email: USERNAME_CONTS, PASSWORD_CONTS }, "aaaaa");
    };

    fetchData();
  }, []);
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <DataTableSalesOrderHeader
          toggleRegisterSalesOrder={toggleRegisterSalesOrder}
          toggleUpdateSalesOrder={toggleUpdateSalesOrder}
          toggleViewConfirmSalesOrder={toggleViewConfirmSalesOrder}
          toggleViewCancelSalesOrder={toggleViewCancelSalesOrder}
        />
      </GridItem>
      <FormRegisterSalesOrderDialog
        modalRegisterSalesOrder={modalRegisterSalesOrder}
        toggleRegisterSalesOrder={toggleRegisterSalesOrder}
      />
      <FormUpdateSalesOrderDialog
        modalUpdateSalesOrder={modalUpdateSalesOrder}
        toggleUpdateSalesOrder={toggleUpdateSalesOrder}
      />
      <ViewConfirmSalesOrderDialog
        modalViewConfirmSalesOrder={modalViewConfirmSalesOrder}
        toggleViewConfirmSalesOrder={toggleViewConfirmSalesOrder}
        toggleExtendPayment={toggleExtendPayment}
      />
      <ViewCancelSalesOrderDialog
        modalViewCancelSalesOrder={modalViewCancelSalesOrder}
        toggleViewCancelSalesOrder={toggleViewCancelSalesOrder}
        toggleAskingCancel={toggleAskingCancel}
      />
      <ExtendPaymentDialog
        modalExtendPayment={modalExtendPayment}
        toggleExtendPayment={toggleExtendPayment}
      />

      <AskingCanceltDialog
        modalAskingCancel={modalAskingCancel}
        toggleAskingCancel={toggleAskingCancel}
      />
    </GridContainer>
  );
}

const mapStateToProps = ({ authUser }) => {
  const { user, loading, error, token } = authUser;
  return { user, loading, error, token };
};

export default connect(mapStateToProps, {
  loginUser,
})(SalesManagement);
