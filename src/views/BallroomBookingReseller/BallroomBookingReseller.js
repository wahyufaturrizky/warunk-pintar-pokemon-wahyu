import React, { useState, useEffect } from "react";
// @material-ui/core Components

// core Components
import GridItem from "../../Components/Grid/GridItem.js";
import GridContainer from "../../Components/Grid/GridContainer.js";
//
import DataTableBallroomBookingResellerHeader from "../../Components/DataTableBallroomBookingResellerHeader";
import FormBallroomBookingResellerDetailDialog from "../../Components/FormBallroomBookingResellerDetailDialog";
import ViewBallroomBookingResellerDialog from "../../Components/ViewBallroomBookingResellerDialog";
import FormPaymentBallroomBookingResellerDetailDialog from "../../Components/FormPaymentBallroomBookingResellerDetailDialog";
import {
  URL_IRINA,
  URL_ZARA,
  USERNAME_CONTS,
  PASSWORD_CONTS,
} from "../../config";
import { connect } from "react-redux";
import { loginUser } from "../../redux/actions";
import axios from "axios";

function BallroomBookingReseller(props) {
  const [modal, setModal] = useState(false);
  const [OpenAlertProductManagement, setOpenAlertProductManagement] = useState(
    false
  );
  const [modalViewBookingReseller, setmodalViewBookingReseller] = useState(
    false
  );
  const [
    modalPaymentBookingResellerDetail,
    setmodalPaymentBookingResellerDetail,
  ] = useState(false);

  const [
    modalViewBookingResellerDetail,
    setModalViewBookingResellerDetail,
  ] = useState(false);

  const [formState, setFormState] = useState({
    status: "",
  });

  const [formStateHalalCert, setFormStateHalalCert] = useState({
    halal_cert_no: "",
    certifying_body: "",
    halalcert_id: "",
    expiry_date: new Date(),
  });

  // const onDeleteHalalCert = (e) => {
  //   const { halalcert_id } = formStateHalalCert;
  //   e.preventDefault();
  //   axios
  //     .delete(`${URL_ZARA}/halalcert/${halalcert_id}`)
  //     .then((result) => {
  //       if (result.status === 200) {
  //         setDeleteHalalCert(true);
  //       }
  //     })
  //     .catch((err) => console.log(err));
  // };

  const handleTextFieldChange = (e) => {
    setFormState({
      ...formState,
      [e.target.name]: e.target.value,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    axios
      .put(
        `${URL_IRINA}/product`,
        {
          ...formState,
        },
        {
          headers: {
            Authorization: `Bearer ${props.token}`,
          },
        }
      )
      .then((result) => {
        if (result.status === 200) {
          // setModal(false);
          setOpenAlertProductManagement(true);
          console.log("success", result);
        }
      })
      .catch((err) => console.log("error", err));
  };

  const toggle = (e) => {
    setModal(!modal);
    setFormState(e);
  };

  const toggleViewBookingReseller = () => {
    setmodalViewBookingReseller(!modalViewBookingReseller);
  };

  const toggleViewBookingResellerDetail = () => {
    setModalViewBookingResellerDetail(!modalViewBookingResellerDetail);
  };

  const togglePaymentBookingResellerDetail = () => {
    setmodalPaymentBookingResellerDetail(!modalPaymentBookingResellerDetail);
  };

  useEffect(() => {
    const fetchData = async () => {
      props.loginUser({ email: USERNAME_CONTS, PASSWORD_CONTS }, "aaaaa");
    };

    fetchData();
  }, []);
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <DataTableBallroomBookingResellerHeader
          toggleViewBookingReseller={toggleViewBookingReseller}
        />
      </GridItem>
      {/* ----- [START VIEW CALENDAR BOOKING TIME SLOT] ----- */}
      <ViewBallroomBookingResellerDialog
        modalViewBookingReseller={modalViewBookingReseller}
        toggleViewBookingReseller={toggleViewBookingReseller}
        toggleViewBookingResellerDetail={toggleViewBookingResellerDetail}
      />
      {/* ----- [END VIEW CALENDAR BOOKING TIME SLOT] ----- */}
      {/* ----- [] ----- */}
      {/* ----- [START ADD AND VIEW DETAIL BALLROOM] ----- */}
      <FormBallroomBookingResellerDetailDialog
        modalViewBookingResellerDetail={modalViewBookingResellerDetail}
        toggleViewBookingResellerDetail={toggleViewBookingResellerDetail}
        togglePaymentBookingResellerDetail={togglePaymentBookingResellerDetail}
      />
      {/* ----- [END ADD AND VIEW DETAIL BALLROOM] ----- */}
      {/* ----- [] ----- */}
      <FormPaymentBallroomBookingResellerDetailDialog
        modalPaymentBookingResellerDetail={modalPaymentBookingResellerDetail}
        togglePaymentBookingResellerDetail={togglePaymentBookingResellerDetail}
      />
    </GridContainer>
  );
}

const mapStateToProps = ({ authUser }) => {
  const { user, loading, error, token } = authUser;
  return { user, loading, error, token };
};

export default connect(mapStateToProps, {
  loginUser,
})(BallroomBookingReseller);
