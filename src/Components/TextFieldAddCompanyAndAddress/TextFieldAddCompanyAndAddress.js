import React, { useState, useEffect } from "react";
import { Box, TextField, Grid, InputLabel } from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import axios from "axios";
import { URL_ANASTASIA, USERNAME_CONTS, PASSWORD_CONTS } from "../../config";
import { connect } from "react-redux";
import { loginUser } from "../../redux/actions";

const TextFieldAddCompanyAndAddress = (props) => {
  const [dataCompany, setDataCompany] = useState([]);

  useEffect(() => {
    const fetchDataToken = async () => {
      props.loginUser({ email: USERNAME_CONTS, PASSWORD_CONTS }, "aaaaa");
    };
    const fetchData = async () => {
      const result = await axios(`${URL_ANASTASIA}/companies`, {
        headers: {
          Authorization: `${props.token}`,
        },
      });
      setDataCompany(result.data);
    };

    fetchData();
    fetchDataToken();
  }, []);

  return (
    <Box>
      {[0].map((key, index) => (
        <Box key={index}>
          <InputLabel shrink>Company Name and Address</InputLabel>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <Autocomplete
                disabled={props.disabled ? props.disabled : null}
                id='company_name'
                required={true}
                onChange={props.onTagsChangeCompany}
                options={dataCompany.map((option) => option.company_name)}
                // getOptionLabel={(option) => option.company_name}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label='Company Name'
                    variant='standard'
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                disabled={props.disabled ? props.disabled : null}
                type='text'
                required={true}
                name='address'
                label='Address'
                id='address'
                onChange={(e) => props.handleTextFieldChange(e)}
              />
            </Grid>
          </Grid>
        </Box>
      ))}
    </Box>
  );
};

const mapStateToProps = ({ authUser }) => {
  const { user, loading, error, token } = authUser;
  return { user, loading, error, token };
};

export default connect(mapStateToProps, { loginUser })(
  TextFieldAddCompanyAndAddress
);
