import React, { useState, useRef, useCallback } from "react";
import "./FormBallroomBookingResellerDetailImage.css";
import "../../node_modules/react-image-gallery/styles/css/image-gallery.css";
import {
  TextField,
  Container,
  Collapse,
  InputLabel,
  Select,
  MenuItem,
  LinearProgress,
  Box,
  Typography,
  FormControl,
  IconButton,
} from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import { Alert } from "@material-ui/lab";
import {
  makeStyles,
  ThemeProvider,
  createMuiTheme,
} from "@material-ui/core/styles";
import { URL_IRINA, USERNAME_CONTS, PASSWORD_CONTS } from "../config";
import axios from "axios";
import Button from "./CustomButtons/Button.js";
import ImageIcon from "@material-ui/icons/Image";
import Check from "@material-ui/icons/Check";
import { connect } from "react-redux";
import { loginUser } from "../redux/actions";

// Icons Material UI
import Visibility from "@material-ui/icons/Visibility";
import Book from "@material-ui/icons/Book";
import AccountBalance from "@material-ui/icons/AccountBalance";
import Person from "@material-ui/icons/Person";
import EmojiPeople from "@material-ui/icons/EmojiPeople";
import PhoneAndroid from "@material-ui/icons/PhoneAndroid";
import LocationCity from "@material-ui/icons/LocationCity";
import CardTravel from "@material-ui/icons/CardTravel";
import LocalActivity from "@material-ui/icons/LocalActivity";
import DeleteIcon from "@material-ui/icons/Delete";
import AddBox from "@material-ui/icons/AddBox";
import Add from "@material-ui/icons/Add";
import AddShoppingCart from "@material-ui/icons/AddShoppingCart";
import PeopleOutline from "@material-ui/icons/PeopleOutline";
import DateRange from "@material-ui/icons/DateRange";

// React  Image galery
import ImageGallery from "react-image-gallery";

import InputAdornment from "@material-ui/core/InputAdornment";
import { orange } from "@material-ui/core/colors";

const PREFIX_URL =
  "https://raw.githubusercontent.com/xiaolin/react-image-gallery/master/static/";

const useStyles = makeStyles((theme) => ({
  inputMd: {
    width: 400,
  },
  input: {
    display: "none",
  },
}));

const theme = createMuiTheme({
  palette: {
    primary: orange,
  },
});

function LinearProgressWithLabel(props) {
  return (
    <Box display='flex' alignItems='center'>
      <Box width='100%' mr={1}>
        <LinearProgress variant='determinate' {...props} />
      </Box>
      <Box minWidth={35}>
        <Typography variant='body2' color='textSecondary'>{`${Math.round(
          props.value
        )}%`}</Typography>
      </Box>
    </Box>
  );
}

const FormBallroomBookingResellerDetailImageTest = (props) => {
  const classes = useStyles();

  const [formState, setFormState] = useState({
    product_name: "",
    brand_name: "",
    category: "",
    risk_level: "",
    status: "",
    imageProduct: [],
  });

  const [dataProducts, setDataProducts] = useState([]);
  const [openAddImageProductDialog, setOpenAddImageProductDialog] = useState(
    false
  );

  const [openAlertConflict, setOpenAlertConflict] = React.useState(false);
  const [openAlertSuccess, setOpenAlertSuccess] = React.useState(false);
  const [openAlertError, setOpenAlertError] = React.useState(false);

  const [progress, setProgress] = React.useState(0);
  const [progressShow, setProgressShow] = React.useState(false);

  const [values, setValues] = useState({ val: [] });
  const [nameVariation, setNameVariation] = useState("");

  const onSubmit = (e) => {
    e.preventDefault();
    onStartUpload(40);

    const dataAllProduct = new FormData();
    // dataAllProduct.append("product_name", product_name);
    // dataAllProduct.append("brand_name", brand_name);
    // dataAllProduct.append("category", category);
    // dataAllProduct.append("risk_level", risk_level);
    // dataAllProduct.append("status", status);
    // imageProduct.map((key) => dataAllProduct.append("imageProduct", key));

    axios
      .post(`${URL_IRINA}/product`, dataAllProduct, {
        headers: {
          Authorization: `Bearer ${props.token}`,
        },
      })
      .then((result) => {
        console.log("result = ", result);
        if (result.status === 200) {
          setOpenAlertSuccess(true);
          onStartUpload(100);
        }
      })
      .catch((err) => {
        console.log("error submit halal cert = ", err.response);
        if (err.response.status === 404) {
          setOpenAlertError(true);
        }
        if (err.response.status === 409) {
          setOpenAlertConflict(true);
        }
      });
  };

  const onStartUpload = (value) => {
    setProgress(value);
    setProgressShow(true);
    if (value === 100) {
      setTimeout(() => {
        setProgress(value);
        setProgressShow(false);
      }, 2000);
    }
  };

  const onChangeProductFile = (files) => {
    console.log("files", files);
    setFormState({
      ...formState,
      imageProduct: files,
    });
  };

  React.useEffect(() => {
    const fetchDataToken = async () => {
      props.loginUser({ email: USERNAME_CONTS, PASSWORD_CONTS }, "aaaaa");
    };
    const fetchData = async () => {
      const result = await axios(`${URL_IRINA}product/questionnaire`, {
        headers: {
          Authorization: `Bearer ${props.token}`,
        },
      });
      console.log("setDataProducts = ", result.data);
      setDataProducts(result.data);
    };

    fetchData();
    fetchDataToken();
  }, []);

  const images = [
    {
      original: "https://picsum.photos/id/1018/1000/600/",
      thumbnail: "https://picsum.photos/id/1018/250/150/",
    },
    {
      original: "https://picsum.photos/id/1015/1000/600/",
      thumbnail: "https://picsum.photos/id/1015/250/150/",
    },
    {
      original: "https://picsum.photos/id/1019/1000/600/",
      thumbnail: "https://picsum.photos/id/1019/250/150/",
    },
  ];

  return (
    <Container>
      <form onSubmit={onSubmit}>
        <ImageGallery items={images} />
        <Collapse in={openAlertSuccess}>
          <Alert
            severity='success'
            onClose={() => {
              setOpenAlertSuccess(false);
            }}
          >
            Success Created product_name !
          </Alert>
          <br />
        </Collapse>
        <Collapse in={openAlertConflict}>
          <Alert
            severity='error'
            onClose={() => {
              setOpenAlertConflict(false);
            }}
          >
            Failed Created product_name, Duplicated product_name !
          </Alert>
          <br />
        </Collapse>
        <Collapse in={openAlertError}>
          <Alert
            severity='error'
            onClose={() => {
              setOpenAlertError(false);
            }}
          >
            Failed Created product_name !
          </Alert>
          <br />
        </Collapse>
        {progressShow && <LinearProgressWithLabel value={progress} />}
        <Button startIcon={<Check />} color='whashalal' type='submit'>
          SUBMIT
        </Button>
      </form>
    </Container>
  );
};

const mapStateToProps = ({ authUser }) => {
  const { user, loading, error, token } = authUser;
  return { user, loading, error, token };
};

export default connect(mapStateToProps, { loginUser })(
  FormBallroomBookingResellerDetailImageTest
);
