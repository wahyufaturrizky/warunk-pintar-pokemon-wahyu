import axios from "axios";

export default class ProductService {
  getProductsSmall() {
    return axios.get("data/products-small.json").then((res) => res.data.data);
  }

  getProducts() {
    return axios
      .get("https://pokeapi.co/api/v2/pokemon?limit=1050")
      .then((res) => {
        console.log("getProducts", res);
        return res;
      });
  }

  getProductsDetail(e) {
    return axios.get(e).then((res) => {
      console.log("getProductsDetail", res);
      return res;
    });
  }

  getProductsWithOrdersSmall() {
    return axios
      .get("data/products-orders-small.json")
      .then((res) => res.data.data);
  }
}
