"use strict";
const dotenv = require("dotenv");
dotenv.config();

module.export = {
  URL_ANASTASIA: process.env.API_URL_ANASTASIA,
  URL_IRINA: process.env.API_URL_IRINA,
  URL_ZARA: process.env.API_URL_ZARA,
  USERNAME_CONTS: process.env.USERNAME,
  PASSWORD_CONTS: process.env.PASSWORD,
};
// // [ANASTASIA]
// export const URL_ANASTASIA = process.env.API_URL_ANASTASIA;
// // [IRINA]
// export const URL_IRINA = process.env.API_URL_IRINA;
// // [ZARA]
// export const URL_ZARA = process.env.API_URL_ZARA;
// // [USERNAME]
// export const USERNAME_CONTS = process.env.USERNAME;
// // [PASSWORD]
// export const PASSWORD_CONTS = process.env.PASSWORD;

// Auth Token
// export const username = "customer_alpha@whatshalal.com";
// export const password = "WH_p@ssw0rd1234!@#$";

// [BACKAND GA-ADMIN]
// export const baseURLProducts = "http://localhost:5000/ingredients";
// export const baseURLBrandName = "http://localhost:5000/ingredients";
// export const baseURLQuestionaires = "http://localhost:5000/questionnaires";
// export const baseURLManufactures = "http://localhost:5000/manufacturers";
// export const baseURLSuppliers = "http://localhost:5000/suppliers";

// export const baseURLCompanies = "http://localhost:5000/companies";
// export const baseURLAuser = "http://localhost:5000/user";
// export const baseURL = "http://localhost:5000/organization";
// export const baseURLSearchOrganization =
//   "http://localhost:5000/organization/search";
// export const baseURLSearchSupplier = "http://localhost:5000/supplier/search";
// export const baseURLSearchManufacture =
//   "http://localhost:5000/manufacturer/search";
// export const baseURLSearchProduct = "http://localhost:5000/ingredients/search";

// [ANASTASIA]
// export const baseURLAnastasia = "https://stg-anastasia.whatshalal.com";
// export const baseURLAuser =
//   "https://stg-anastasia.whatshalal.com/user/register";
// export const baseURL = "https://stg-anastasia.whatshalal.com/organization";
// export const baseURLSearchOrganization =
//   "https://stg-anastasia.whatshalal.com/organization/search";
// export const baseURLSearchManufacture =
//   "https://stg-anastasia.whatshalal.com/manufacturer/search";
// export const baseURLSearchSupplier =
//   "https://stg-anastasia.whatshalal.com/supplier/search";
// export const baseURLManufactures =
//   "https://stg-anastasia.whatshalal.com/manufacturers";
// export const baseURLCompanies =
//   "https://stg-anastasia.whatshalal.com/companies";
// export const baseURLSuppliers =
//   "https://stg-anastasia.whatshalal.com/suppliers";
// export const baseURLQuestionaires =
//   "https://stg-anastasia.whatshalal.com/questionnaires";

// [IRINA]
// export const baseURLIrinaGetAllProduct =
//   "https://stg-irina.whatshalal.com/api/product/questionnaire";
// export const baseURLProducts = "https://stg-irina.whatshalal.com/api/product";
// export const baseURLSearchProduct =
//   "https://stg-irina.whatshalal.com/api/product/questionnaire/search";

// // [ZARA]
// export const baseURLHalalCert = "https://stg-zara.whatshalal.com/halalcerts";
// export const baseURLSearchHalalCert =
//   "https://stg-zara.whatshalal.com/halalcert/search";
// export const baseURLRegisterHalalCert =
//   "https://stg-zara.whatshalal.com/halalcert";
// export const baseURLViewImageHalalCertById =
//   "https://stg-zara.whatshalal.com/view/halalcert";
// export const baseURLDownloadImageHalalCertById =
//   "https://stg-zara.whatshalal.com/download/halalcert";
